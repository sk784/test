import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../../Icons/custom_icons_icons.dart';
import 'Place.dart';


class PhotoGrid extends StatelessWidget {
  Place place;


  PhotoGrid(this.place);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(crossAxisCount: 3,
          padding: EdgeInsets.only(top: 16.0),
          crossAxisSpacing: 2.0,
          mainAxisSpacing: 2.0,
          children: List.generate(place.customImages.length, (index){
            return InkWell(
              child: Container(
                child: FadeInImage.memoryNetwork(placeholder: kTransparentImage, image: place.customImages[index].fullImage,
                  fit: BoxFit.cover,
                ),
              ),
              onTap: (){
                Navigator.push(context, CupertinoPageRoute(builder: (_) {
                  return DetailScreen(place,index);
                }));
              },
            );
          })
      ),
    );
  }
}

class DetailScreen extends StatefulWidget {

  final Place place;
  final int index;
  DetailScreen(this.place,this.index);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen>{

  int _photoIndex;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _photoIndex = widget.index;
    _pageController = PageController(initialPage:  _photoIndex, keepPage: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: 120,
                  child: DragTarget(builder:
                      (context, List<Place> cd, rd) {
                    return Container();
                  },
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (data) {
                        Navigator.pop(context);
                      }
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: 400,
                  child: Stack(
                   children: [
                    Draggable(
                      data:widget.place,
                      feedback: SizedBox(
                        height: 400,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: Opacity(
                          opacity: 0.5,
                          child: PageView.builder(
                   pageSnapping: true,
                    itemCount: widget.place.customImages.length,
                            controller: _pageController,
                    onPageChanged: (int index) {
                          setState(() {
                            _photoIndex =  index;
                          });
                     },
                    itemBuilder: (context, index) {
                          return PhotoView(imageProvider: FadeInImage
                              .memoryNetwork(placeholder: kTransparentImage,
                              image: widget.place.customImages[_photoIndex].fullImage)
                              .image,
                            backgroundDecoration: BoxDecoration(
                                color: Colors.white.withAlpha(120)),
                            minScale: PhotoViewComputedScale.contained,
                            maxScale: PhotoViewComputedScale.covered * 1.1,);
                    },
                   ),
                        ),
                      ),
                      childWhenDragging: Container(),
                      child: SizedBox(
                        height: 400,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: PageView.builder(
                          pageSnapping: true,
                          itemCount: widget.place.customImages.length,
                          controller: _pageController,
                          onPageChanged: (int index) {
                            setState(() {
                              _photoIndex = index;
                            });
                          },
                          itemBuilder: (context, index) {
                            return PhotoView(imageProvider: FadeInImage
                                .memoryNetwork(placeholder: kTransparentImage,
                                image:widget.place.customImages[_photoIndex].fullImage)
                                .image,
                              backgroundDecoration: BoxDecoration(
                                  color: Colors.white.withAlpha(120)),
                              minScale: PhotoViewComputedScale.contained,
                              maxScale: PhotoViewComputedScale.covered * 1.1,);
                          },
                        ),
                      ),
                    ),
                     Container(
                         alignment: Alignment.topLeft,
                         margin: EdgeInsets.only(left: 16),
                         child: FloatingActionButton(
                           onPressed: () async {
                             Navigator.pop(context);
                           },
                           materialTapTargetSize: MaterialTapTargetSize.padded,
                           backgroundColor: Colors.white,
                           child: Padding(
                             padding: EdgeInsets.only(right: 1.0),
                             child: const Icon(Icons.arrow_back_ios,
                                 color: Color.fromARGB(255, 47, 128, 237),
                                 size: 24.0),
                           ),
                         )
                     )
                  ],
                ),
              )
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height - 520,
                  child: DragTarget(builder:
                      (context, List<Place> cd, rd) {
                    return Container();
                  },
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (data) {
                        Navigator.pop(context);
                      }
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}